//
//  InfoView.swift
//  MountainsAR
//
//  Created by Apple on 9/26/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class InfoView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var view: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageIndicator: UIActivityIndicatorView!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var title: UILabel!
    
    override init(frame: CGRect) {
        // setup any properties here
        
        // call super.init(frame:)
        super.init(frame:frame)
        
        //setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        //setup any properties here
        
        //call super.init(frame:)
        super.init(coder: aDecoder)
        
        // setup view from .xib file
        xibSetup()
    }
    
    func xibSetup(){
        view =  loadViewFromNib()
        
        //user bounds not frame or it'll be offset
        view.frame = bounds
        
        // make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "InfoView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view

    }
    
    override func layoutSubviews() {
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        descTextView.contentOffset = CGPointZero
    }
    

}
