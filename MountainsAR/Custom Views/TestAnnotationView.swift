//
//  TestAnnotationView.swift
//  HDAugmentedRealityDemo
//
//  Created by Danijel Huis on 30/04/15.
//  Copyright (c) 2015 Danijel Huis. All rights reserved.
//

import UIKit
import AFMActionSheet
import AlamofireImage
import Alamofire
import Localize_Swift

public class TestAnnotationView: ARAnnotationView, UIGestureRecognizerDelegate
{
    public var titleLabel: UILabel?
    public var infoButton: UIButton?
    public var myView:UIViewController!
    public var placeTypeText = ""
    var customView = InfoView(frame: CGRectMake(0,0,130,200))
    public var mountainHeight:String="Unknown"
    override public func didMoveToSuperview()
    {
        super.didMoveToSuperview()
        if self.titleLabel == nil
        {
            self.loadUi()
        }
    }
    
    func loadUi()
    {
        // Title label
        self.titleLabel?.removeFromSuperview()
        let label = UILabel()
        label.font = UIFont.systemFontOfSize(10)
        label.numberOfLines = 0
        label.backgroundColor = UIColor.clearColor()
        label.textColor = UIColor.whiteColor()
        self.addSubview(label)
        self.titleLabel = label
        
        // Info button
        self.infoButton?.removeFromSuperview()
        let button = UIButton(type: UIButtonType.DetailDisclosure)
        button.userInteractionEnabled = false   // Whole view will be tappable, using it for appearance
        self.addSubview(button)
        self.infoButton = button
        
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("tapGesture"))
        self.addGestureRecognizer(tapGesture)
        
        // Other
        self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        self.layer.cornerRadius = 5
        if self.annotation != nil
        {
            self.bindUi()
        }
    }
    
    func layoutUi()
    {
        let buttonWidth: CGFloat = 40
        let buttonHeight: CGFloat = 40
        
        self.titleLabel?.frame = CGRectMake(10, 0, self.frame.size.width - buttonWidth - 5, self.frame.size.height);
        self.infoButton?.frame = CGRectMake(self.frame.size.width - buttonWidth, self.frame.size.height/2 - buttonHeight/2, buttonWidth, buttonHeight);
    }
    
    // This method is called whenever distance/azimuth is set
    override public func bindUi()
    {
        if let annotation = self.annotation, let title = annotation.title
        {
            let prefs = NSUserDefaults.standardUserDefaults()
            let unitsType = prefs.stringForKey("units")
            let distance:String!
            
            if unitsType == UnitsType.m.rawValue{
                distance = annotation.distanceFromUser > 1000 ? String(format: "%.1f miles", annotation.distanceFromUser * 0.000621371) : String(format:"%.0fm", annotation.distanceFromUser)
            } else {
                distance = annotation.distanceFromUser > 1000 ? String(format: "%.1fkm", annotation.distanceFromUser / 1000) : String(format:"%.0fm", annotation.distanceFromUser)
            }
            if let placeType = annotation.type{
                placeTypeText = placeType
            } else {
                placeTypeText = "Unknown".localized()
            }
            if let height = annotation.height{
               mountainHeight = height
            } else {
                mountainHeight = "Unknown"
            }
            
            let text = String(format: "%@\nHeight: %@\nDST: %@", title, mountainHeight, distance)
            self.titleLabel?.text = text
        }
    }
    
    public override func layoutSubviews()
    {
        super.layoutSubviews()
        self.layoutUi()
    }
    
    public func tapGesture()
    {
        if let annotation = self.annotation
        {
           // customView.descTextView.scrollRangeToVisible(NSMakeRange(0, 0))
            customView.title.text = annotation.title
            if(annotation.desc != ""){
                customView.descTextView.text = annotation.desc
            } else {
                customView.descTextView.text = "No Description Available".localized()
            }
            
            setImage(annotation,customView: customView)
            
            let actionSheet = AFMActionSheetController()
            let action = AFMAction(title: NSLocalizedString("Know More".localized(), comment: ""), enabled: true, handler: { (action: AFMAction) -> Void in
                self.setWebLink(annotation)
            })
            
            let cancelAction = AFMAction(title: NSLocalizedString("Cancel".localized(), comment: ""), enabled: true, handler: nil)
            actionSheet.addCancelAction(cancelAction)
            actionSheet.addTitleView(customView)
            actionSheet.addAction(action)
            self.parentViewController?.presentViewController(actionSheet, animated: true, completion: nil)
        }
    }
    
    func setImage(annotation: ARAnnotation, customView: InfoView){
        
        if let imageName = annotation.imageName {
            
            let file = fileInDocumentsDirectory(imageName)
            let fileManager = NSFileManager.defaultManager()
            //check if image file is available in documents directory
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    customView.imageView.image = imageContent
                } else {
                    customView.imageView.image = UIImage(named: "no_image")
                }
                customView.imageIndicator.hidesWhenStopped = true
            } else {
                let imageURL = IMAGE_PATH + imageName
                customView.imageIndicator.startAnimating()
                
                Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                        if let image = response.result.value
                        {
                            customView.imageView.image = image
                            self.saveImage(image, image_name: imageName)
                        }
                        else
                        {
                            customView.imageView.image = UIImage(named: "no_image")
                        }
                        customView.imageIndicator.stopAnimating()
                        customView.imageIndicator.hidesWhenStopped = true
                }

            }
        } else {
            customView.imageView.image = UIImage(named: "no_image")
        }
    }
    
    public func setWebLink(annotation: ARAnnotation){
        if let webLink = annotation.webLink {
            if let url = NSURL(string: webLink){
                UIApplication.sharedApplication().openURL(url)
            }
        } else {
            let alertController = UIAlertController(title: "Link Not Available", message: "Sorry, no links are available for this Place", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.parentViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            print("File Name: \(filename)")
            data.writeToFile(filename, atomically: true)
        }
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }

}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.nextResponder()
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
