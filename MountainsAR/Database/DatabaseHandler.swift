//
//  DatabaseHandler.swift
//  MountainsAR
//
//  Created by Apple on 10/3/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DatabaseHandler {
    let appDelegate : AppDelegate!
    let managedContext : NSManagedObjectContext!
    let entityName:String!
    
    init(entityName:String!){
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext
        self.entityName = entityName
    }
    
    func savePlaces(places: NSObject){
            let data = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: managedContext) as! PlacesEntity
        
            let encodedPlaces = NSKeyedArchiver.archivedDataWithRootObject(places)
            data.places = encodedPlaces
        
        do{
            try managedContext.save()
        }catch let error as NSError{
            print("Couldn't Save \(error.localizedDescription)")
        }
    }
    
    func getPlaces() -> [PlacesEntity]{
        var placesData: [PlacesEntity] = []
        let fetchRequest = NSFetchRequest(entityName: entityName)
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest) as! [PlacesEntity]
            placesData = results
            return placesData
        } catch let error as NSError{
            print("couldn't fetch \(error.localizedDescription)")
        }
        return placesData
    }
    
    
    func clearData(){
        let coord = appDelegate.persistentStoreCoordinator
        let fetchRequest = NSFetchRequest(entityName: entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do{
            try coord.executeRequest(deleteRequest, withContext: managedContext)
        } catch {
            print("Couldn't Delete")
        }

    }
}