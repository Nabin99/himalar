//
//  PlacesEntity+CoreDataProperties.swift
//  MountainsAR
//
//  Created by Apple on 10/17/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PlacesEntity {

    @NSManaged var places: NSData?

}
