//
//  CustomLoader.swift
//  MountainsAR
//
//  Created by Apple on 9/27/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import SwiftLoader

class CustomLoaderConfig : NSObject {
    
    var config: SwiftLoader.Config = SwiftLoader.Config()
    
    override init(){
        config.size = 150
        config.spinnerColor = UIColor.whiteColor()
        config.backgroundColor = UIColor.blackColor()
        config.foregroundAlpha = 0.5
        config.titleTextColor = UIColor.whiteColor()
        SwiftLoader.setConfig(config)
    }
}