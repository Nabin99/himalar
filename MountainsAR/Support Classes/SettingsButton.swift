//
//  SettingsButton.swift
//  MountainsAR
//
//  Created by Apple on 10/20/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import UIKit

class SettingsButton: NSObject {
    
    var currentView:UIViewController!
    var settingsButton : UIButton!
    
    init(currentView:UIViewController){
        self.currentView = currentView
    }
    
    convenience init(cView:UIViewController){
        self.init(currentView: cView)
        settingsButton = UIButton(type: UIButtonType.Custom)
        settingsButton.setImage(UIImage(named: "settings"), forState: .Normal)
        settingsButton.frame = CGRect(x: currentView.view.bounds.size.width - 45, y: 20,width: 40,height: 40)
        settingsButton.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleBottomMargin]
        settingsButton.addTarget(self, action: #selector(showSettingsController), forControlEvents: UIControlEvents.TouchUpInside)
        currentView.view.addSubview(settingsButton)
    }
    
    func showSettingsController(){
        let settingsController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("settings") as! SettingsController
        currentView.presentViewController(settingsController, animated: true, completion: nil)
    }
}
