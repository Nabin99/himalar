//
//  PlacesLoader.swift
//  MountainsAR
//
//  Created by Apple on 9/14/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import MapKit
import Alamofire
import JLToast
import SwiftyJSON
import SwiftLoader
import Localize_Swift


class PlacesLoader : NSObject, ARDataSource{
    
    let myLocation:CLLocation!
    let radius = 1000
    let places = "school"
    var placesList:Array<Places> = Array()
    var mapView : MKMapView!
    var annotations:[MKPointAnnotation] = Array()
    var myView: UIViewController
    var updatedPlaceList:Array<Places> = Array()
    var pinAnnotationView: MKPinAnnotationView!
    
    init(myView: UIViewController,myLocation:CLLocation) {
        self.myLocation = myLocation
        self.myView = myView
    }
    
    init(myView: UIViewController, myLocation:CLLocation, mapView:MKMapView){
        self.myLocation = myLocation
        self.mapView = mapView
        self.myView = myView
    }
    
    // Get Nearby Points of Interest
//    func getNearByPlaces(){
//    
//        let url = GOOGLE_PLACES_URL + "nearbysearch/json?location=\(myLocation.coordinate.latitude),\(myLocation.coordinate.longitude)&radius=\(radius)&sensor=true&key=\(API_KEY)"
//        print(url)
//        Alamofire.request(.GET,url)
//            .responseJSON{ response in
//                switch response.result{
//                case .Success:
//                    if let value = response.result.value {
//                        let json = JSON(value)
//                        self.parseResponse(json)
//                    }
//                case .Failure(let error):
//                    JLToast.makeText(error.localizedDescription).show()
//                }
//        }
//    }
    
    func getPlaces(){
        
        let placesArray = getPlacesFromDB()   // gets data from the local storage
        
        if(placesArray.count > 0){   // if Saved data is available
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
                self.updateData();
            }
            
            for (var i=0; i<placesArray.count; i++){
                let data = placesArray[i].places!
                let decodedData = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Places
                let places: Places = Places(id: decodedData.getId(), name: decodedData.getName(), type: decodedData.getType(), location: decodedData.getLocation(), desc: decodedData.getDescription(), imageName: decodedData.getImageName(), webLink: decodedData.getWebLink(), height: decodedData.getHeight())
                placesList.append(places)
            }
            addAnnotations()
        } else {
        // gets all data from the server
            let _ = CustomLoaderConfig()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            SwiftLoader.show(title: NSLocalizedString("Loading", comment: ""), animated: true)
            Alamofire.request(.GET,GET_PLACES)
                .responseJSON { response in
                    switch response.result{
                    case .Success:
                        if let value = response.result.value{
                            let json = JSON(value)
                            self.parsePlaceJSON(json)
                        }
                    case .Failure(let error):
                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: {void in self.getPlaces()}))
                        self.myView.presentViewController(alert, animated: true, completion: nil)
                    }
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    SwiftLoader.hide()
            }
        }
    }

    
    func updateData(){
                    // Check whether the Data on the server has been updated
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
                    Alamofire.request(.GET,GET_PLACES)
                        .responseJSON { response in
                            switch response.result{
                            case .Success:
                                if let value = response.result.value{
                                    let json = JSON(value)
                                    self.parsePlaceJSONToUpdate(json)
                                }
                            case .Failure(let error):
                                print(error.localizedDescription)
                            }
                            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    }

    }
    
    func getPlacesFromDB() -> [PlacesEntity]{
        let data = DatabaseHandler(entityName: "PlacesEntity")
        return data.getPlaces()
    }
    
    func parsePlaceJSON(json: JSON){
        //clear Data From DAtabase
        let databaseHandler = DatabaseHandler(entityName: "PlacesEntity")
        databaseHandler.clearData()
        
        let date = json["version"][0]["info_version"].stringValue
        let updatedDateTime = getNSDateObjectFromString(date)
        
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setDouble(updatedDateTime.timeIntervalSince1970, forKey: "version")
        
        let data = json["data"]
        for(var i=0; i < data.count; i++){
            let latitude = data[i]["latitude"].doubleValue
            let longitude = data[i]["longitude"].doubleValue
            let name = data[i]["title"].stringValue
            let type = data[i]["types"].stringValue
            let imageName = data[i]["image"].stringValue
            let id = data[i]["id"].intValue
            let description = data[i]["description"].stringValue
            let url = data[i]["url"].stringValue
            let height = data[i]["height"].stringValue
            let location:CLLocation = CLLocation(latitude: latitude, longitude: longitude)
            let places: Places = Places(id: id, name: name, type: type, location: location, desc: description, imageName: imageName, webLink: url, height: height)
            databaseHandler.savePlaces(places)
            placesList.append(places)
        }
        addDefaultSettings()
        addAnnotations()
    }
    
    func addDefaultSettings(){
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setValue(true, forKey: "liveView")
        prefs.setValue(MapType.Road.rawValue, forKey: "mapType")
        prefs.setValue(UnitsType.km.rawValue, forKey: "units")
    }
    
    
    func addAnnotations(){
        for i in 0..<placesList.count{
            let annotation:MKPointAnnotation = MKPointAnnotation()
            annotation.coordinate = placesList[i].getLocation().coordinate
            annotation.title = placesList[i].getName()
            annotations.append(annotation)
        }
        let prefs = NSUserDefaults.standardUserDefaults()
        let liveView = prefs.boolForKey("liveView")
        print("LiveView: \(liveView)")
        if liveView == true{
            showARViewController()
        } else {
            showMapViewController()
        }
        
    }
    
    func showMapViewController(){
        let mapViewController = myView.storyboard?.instantiateViewControllerWithIdentifier("mapView") as! MapViewController
        mapViewController.placesList = placesList
        mapViewController.myLocation = myLocation
        myView.presentViewController(mapViewController, animated: true, completion: nil)
    }
    
    func chooseLanguage(){
        
        let availableLanguages = Localize.availableLanguages()

        let actionSheet = UIAlertController(title: nil, message: "Choose Language", preferredStyle: .ActionSheet)
        for language in availableLanguages{
            let displayName = Localize.displayNameForLanguage(language)
            let languageAction = UIAlertAction(title: displayName, style: .Default, handler: {
                (alert: UIAlertAction) -> Void in
                Localize.setCurrentLanguage(language)
                self.showARViewController()
            })
            actionSheet.addAction(languageAction)
        }
        myView.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    
    func showARViewController(){
        //Check if device has hardware needed for augmented reality
        let result = ARViewController.createCaptureSession()
        if result.error != nil {
            let message = result.error?.userInfo["description"] as? String
            let alertView = UIAlertView(title: "Error", message: message, delegate: nil, cancelButtonTitle: "Close")
            alertView.show()
            return
        }
        
        let allAnnotations = getAllAnnotations()
        
        //present ARViewController
        let arViewController = ARViewController()
        arViewController.debugEnabled = true
        arViewController.dataSource = self
        arViewController.maxDistance = 0
        arViewController.maxVisibleAnnotations = 100
        arViewController.maxVerticalLevel = 7
        arViewController.headingSmoothingFactor = 0.05
        arViewController.trackingManager.userDistanceFilter = 25
        arViewController.trackingManager.reloadDistanceFilter = 75
        arViewController.setAnnotations(allAnnotations)
        myView.presentViewController(arViewController, animated: true, completion: nil)
    }
    
    //This method is called by ARViewController, make sure to set dataSource Property
    func ar(arViewController: ARViewController, viewForAnnotation:ARAnnotation) -> ARAnnotationView {
        //Annotation Views should be lightweight views, try to avoid xibs and autolayout all together
        let annotationView = TestAnnotationView()
        annotationView.frame = CGRect(x: 0,y:0,width:150,height: 40)
        return annotationView
    }
    
    func getAllAnnotations() -> Array<ARAnnotation>{
        var arAnnotations : [ARAnnotation] = []
        
        for place in placesList{
            let annotation = ARAnnotation()
            annotation.location = place.getLocation()
            annotation.title = place.getName()
            annotation.type = place.getType()
            annotation.desc = place.getDescription()
            annotation.imageName = place.getImageName()
            annotation.webLink = place.getWebLink()
            annotation.height = place.getHeight()
            arAnnotations.append(annotation)
        }
        return arAnnotations
    }
    
    func parsePlaceJSONToUpdate(json: JSON){
        let date = json["version"][0]["info_version"].stringValue
        let updatedDateTime = getNSDateObjectFromString(date)
        

        let data = json["data"]
        for(var i=0; i < data.count; i++){
            let latitude = data[i]["latitude"].doubleValue
            let longitude = data[i]["longitude"].doubleValue
            let name = data[i]["title"].stringValue
            let type = data[i]["types"].stringValue
            let imageName = data[i]["image"].stringValue
            let id = data[i]["id"].intValue
            let description = data[i]["description"].stringValue
            let url = data[i]["url"].stringValue
            let height = data[i]["height"].stringValue
            
            let location:CLLocation = CLLocation(latitude: latitude, longitude: longitude)
            
            let places: Places = Places(id: id, name: name, type: type, location: location, desc: description, imageName: imageName, webLink: url,height: height)
            updatedPlaceList.append(places)
        }
        
        let prefs = NSUserDefaults.standardUserDefaults()
        
        
        if(prefs.doubleForKey("version") != updatedDateTime.timeIntervalSince1970){
            //do something
            let databaseHandler = DatabaseHandler(entityName: "PlacesEntity")
            //clears all data
            databaseHandler.clearData()
            
            for places in updatedPlaceList{
                databaseHandler.savePlaces(places)
            }
        
        } else {
           print("No new Data")
        }
    }
    
    func getNSDateObjectFromString(date:String) -> NSDate{
        let formatter = NSDateFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.dateFromString(date)
        
        return date!
    }

}
