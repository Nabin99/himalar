//
//  CustomMapAnnotation.swift
//  MountainsAR
//
//  Created by Apple on 10/20/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import MapKit

class CustomMapAnnotation: NSObject, MKAnnotation{
    
    let id:Int
    let name:String
    let coordinate: CLLocationCoordinate2D
    let desc: String
    let imageName:String
    let webLink:String
    let type:String
    let height:String
    
    
    init(id:Int,name:String,coordinate:CLLocationCoordinate2D,desc:String,imageName:String,webLink:String,type:String,height:String) {
        self.id = id
        self.name = name
        self.coordinate = coordinate
        self.desc =  desc
        self.imageName = imageName
        self.webLink = webLink
        self.type = type
        self.height = height
    }
    
    func getId() -> Int{
        return id
    }
    
    func getName() -> String{
        return name
    }
    
    func getCoordinate() -> CLLocationCoordinate2D {
        return coordinate
    }
    
    func getType() -> String {
        return type
    }
    
    func getDescription() -> String{
        return desc
    }
    
    func getImageName() -> String{
        return imageName
    }
    
    func getWebLink() -> String{
        return webLink
    }
    
    func getHeight() -> String {
        return height
    }
}