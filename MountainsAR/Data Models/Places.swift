//
//  Places.swift
//  MountainsAR
//
//  Created by Apple on 9/14/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import Foundation
import MapKit

class Places : NSObject,NSCoding{
    
    var id:Int
    var name:String
    var location: CLLocation
    var desc: String
    var imageName:String
    var webLink:String
    var type:String
    var height:String
    
    init(id:Int,name:String,type:String,location:CLLocation,desc:String,imageName:String,webLink:String,height:String){
        self.id = id
        self.name = name
        self.location = location
        self.type = type
        self.desc = desc
        self.imageName = imageName
        self.webLink = webLink
        self.height = height
    }
    
    func getId() -> Int{
        return id
    }
    
    func getName() -> String{
        return name
    }
    
    func getLocation() -> CLLocation {
        return location
    }
    
    func getType() -> String {
        return type
    }
    
    func getDescription() -> String{
        return desc
    }
    
    func getImageName() -> String{
        return imageName
    }
    
    func getWebLink() -> String{
        return webLink
    }
    
    func getHeight() -> String {
        return height
    }
    
    func encodeWithCoder(archiver: NSCoder) {
        archiver.encodeObject(id,forKey: "id")
        archiver.encodeObject(name, forKey: "name")
        archiver.encodeObject(location, forKey: "location")
        archiver.encodeObject(type, forKey: "type")
        archiver.encodeObject(desc, forKey: "desc")
        archiver.encodeObject(imageName, forKey: "imageName")
        archiver.encodeObject(webLink, forKey: "webLink")
        archiver.encodeObject(height, forKey: "height")
    }
    
    required init(coder unarchiver: NSCoder){
        id = unarchiver.decodeObjectForKey("id") as! Int
        name = unarchiver.decodeObjectForKey("name") as! String
        location = unarchiver.decodeObjectForKey("location") as! CLLocation
        type = unarchiver.decodeObjectForKey("type") as! String
        desc = unarchiver.decodeObjectForKey("desc") as! String
        imageName = unarchiver.decodeObjectForKey("imageName") as! String
        webLink = unarchiver.decodeObjectForKey("webLink") as! String
        height = unarchiver.decodeObjectForKey("height") as! String
        super.init()
    }
    
}
