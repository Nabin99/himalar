//
//  SettingsController.swift
//  MountainsAR
//
//  Created by Apple on 10/19/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit


enum MapType:String{
    case Road,Satellite,Hybrid
}

enum UnitsType:String{
    case km,m
}

class SettingsController: UIViewController {

    //boolean value for live view
    var liveViewValue:Bool!
    var mapTypeValue:String!
    var unitsValue:String!
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var liveViewSwitch: UISwitch!
    
    @IBOutlet weak var mapType: UISegmentedControl!
    
    @IBOutlet weak var Units: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        liveViewSwitch.addTarget(self, action: #selector(SettingsController.stateChanged), forControlEvents: UIControlEvents.ValueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func mapTypeControl(sender: UISegmentedControl) {
        
        switch mapType.selectedSegmentIndex {
        case 0:
            mapTypeValue = MapType.Road.rawValue
            
        case 1:
            mapTypeValue = MapType.Satellite.rawValue
            
        case 2:
            mapTypeValue = MapType.Hybrid.rawValue
            
        default:
            break;
        }
    }
    
    
    @IBAction func unitControl(sender: UISegmentedControl) {
        
        switch Units.selectedSegmentIndex{
        case 0:
            unitsValue = UnitsType.km.rawValue
            
        case 1:
            unitsValue = UnitsType.m.rawValue
            
        default:
            break;
        }
    }
    
    override func viewDidLayoutSubviews() {
        let prefs = NSUserDefaults.standardUserDefaults()
        let liveView = prefs.boolForKey("liveView")
        let maptype = prefs.stringForKey("mapType")
        let unitsType = prefs.stringForKey("units")

        
        var mapIndex:Int
        var unitIndex:Int
        liveViewValue = liveView
        if maptype == "Road"{
                mapIndex = 0
                mapTypeValue = MapType.Road.rawValue
            } else if maptype == "Satellite"{
                mapIndex = 1
                mapTypeValue = MapType.Satellite.rawValue

            } else if maptype == "Hybrid"{
                mapIndex = 2
                mapTypeValue = MapType.Hybrid.rawValue
            
        }else {
            mapIndex = 0
            mapTypeValue = MapType.Road.rawValue
        }
        
        if(unitsType == "km"){
            unitIndex = 0
            unitsValue = UnitsType.km.rawValue
        } else if (unitsType == "m"){
            unitIndex = 1
            unitsValue = UnitsType.m.rawValue
        } else {
            unitIndex = 0
            unitsValue = UnitsType.km.rawValue
        }
        
        liveViewSwitch.setOn(liveView, animated: true)
        mapType.selectedSegmentIndex = mapIndex
        Units.selectedSegmentIndex = unitIndex
    }
    
    
    
    @IBAction func saveAction(sender: UIButton) {
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setValue(liveViewValue, forKey: "liveView")
        prefs.setValue(mapTypeValue, forKey: "mapType")
        prefs.setValue(unitsValue, forKey: "units")
        
        let mainViewController = self.storyboard?.instantiateViewControllerWithIdentifier("mainView") as! ViewController
        presentViewController(mainViewController, animated: true, completion: nil)
       
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func stateChanged(switchState: UISwitch){
        if switchState.on{
            print("Switch is on")
            liveViewValue = true
        } else {
            print ("Switch is off")
            liveViewValue = false
        }
    }
    
    override func viewWillLayoutSubviews() {
        saveBtn.layer.cornerRadius = 5
        saveBtn.clipsToBounds = true
        cancelBtn.layer.cornerRadius = 5
        cancelBtn.clipsToBounds = true
    }
    
}
