//
//  InfoController.swift
//  MountainsAR
//
//  Created by Apple on 10/21/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit

class InfoController: UIViewController {

    @IBOutlet weak var gotItBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillLayoutSubviews() {
        gotItBtn.layer.cornerRadius = 5
        gotItBtn.clipsToBounds = true
        self.automaticallyAdjustsScrollViewInsets = false
    }

    @IBAction func gotItAction(snder: UIButton) {
        
        let prefs = NSUserDefaults.standardUserDefaults()
        prefs.setBool(true, forKey: "InfoRead")
        
        let viewController : UIViewController = self.storyboard?.instantiateViewControllerWithIdentifier("mainView") as! ViewController
        self.presentViewController(viewController, animated: true, completion: nil)
        
    }
}
