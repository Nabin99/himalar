//
//  MapViewController.swift
//  MountainsAR
//
//  Created by Apple on 10/20/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import MapKit
import AFMActionSheet
import Alamofire

class MapViewController: UIViewController,MKMapViewDelegate,Settings{

    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    var placesList:Array<Places> = Array()
    var mapAnnotations:[MKPointAnnotation] = Array()
    var myLocation:CLLocation!
     var customView = InfoView(frame: CGRectMake(0,0,130,200))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        let allAnnotation:[CustomMapAnnotation] = setCustomAnnotation()
        let center = CLLocationCoordinate2D(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
        let region = MKCoordinateRegionMakeWithDistance(center, 100000, 100000)
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true
        
        let prefs = NSUserDefaults.standardUserDefaults()
        let mapType = prefs.stringForKey("mapType")
        if(mapType == MapType.Road.rawValue){
            mapView.mapType = .Standard
        } else if (mapType == MapType.Satellite.rawValue){
            mapView.mapType = .Satellite
        } else if (mapType == MapType.Hybrid.rawValue){
            mapView.mapType = .Hybrid
        }
        mapView.addAnnotations(allAnnotation)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        return nil
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if let customAnnotation = view.annotation as? CustomMapAnnotation{
            customView.title.text = customAnnotation.getName()
            if(customAnnotation.getDescription() != ""){
                customView.descTextView.text = customAnnotation.getDescription()
            } else {
                customView.descTextView.text = "No Description Available".localized()
            }
            
            setImage(customAnnotation,customView: customView)
            
            let actionSheet = AFMActionSheetController()
            let action = AFMAction(title: NSLocalizedString("Know More".localized(), comment: ""), enabled: true, handler: { (action: AFMAction) -> Void in
                self.setWebLink(customAnnotation)
            })
            
            let cancelAction = AFMAction(title: NSLocalizedString("Cancel".localized(), comment: ""), enabled: true, handler: nil)
            actionSheet.addCancelAction(cancelAction)
            actionSheet.addTitleView(customView)
            actionSheet.addAction(action)
            self.presentViewController(actionSheet, animated: true, completion: nil)

        }
    }
    
    func setWebLink(annotation: CustomMapAnnotation){
        if annotation.getWebLink() != "" {
            if let url = NSURL(string: annotation.getWebLink()){
                UIApplication.sharedApplication().openURL(url)
            }
        } else {
            let alertController = UIAlertController(title: "Link Not Available", message: "Sorry, no links are available for this Place", preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            self.parentViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    
    
    func setImage(annotation: CustomMapAnnotation, customView: InfoView){
        
        customView.imageView.image = nil
        if annotation.getImageName() != ""{
            
            let file = fileInDocumentsDirectory(annotation.getImageName())
            let fileManager = NSFileManager.defaultManager()
            //check if image file is available in documents directory
            if(fileManager.fileExistsAtPath(file)){
                let imageContent = UIImage(contentsOfFile: file)
                if(imageContent != nil){
                    customView.imageView.image = imageContent
                } else {
                    customView.imageView.image = UIImage(named: "no_image")
                }
                customView.imageIndicator.hidesWhenStopped = true
            } else {
                let imageURL = IMAGE_PATH + annotation.getImageName()
                customView.imageIndicator.startAnimating()
                
                Alamofire.request(.GET, imageURL)
                    .responseImage{ response in
                        if let image = response.result.value
                        {
                            customView.imageView.image = image
                            self.saveImage(image, image_name: annotation.getImageName())
                        }
                        else
                        {
                            customView.imageView.image = UIImage(named: "no_image")
                        }
                        customView.imageIndicator.stopAnimating()
                        customView.imageIndicator.hidesWhenStopped = true
                }
                
            }
        } else {
            customView.imageView.image = UIImage(named: "no_image")
        }
    }
    
    func saveImage(image:UIImage,image_name:String){
        if let data = UIImagePNGRepresentation(image){
            let filename = getDocumentsDirectory().stringByAppendingPathComponent(image_name)
            print("File Name: \(filename)")
            data.writeToFile(filename, atomically: true)
        }
    }

    func setCustomAnnotation() -> [CustomMapAnnotation]{
        var annotations : [CustomMapAnnotation] = []
        for place in placesList{
            let coordinateS = CLLocationCoordinate2D(latitude: place.getLocation().coordinate.latitude, longitude: place.getLocation().coordinate.longitude)
            let annotation = CustomMapAnnotation(id: place.getId(), name: place.getName(), coordinate: coordinateS, desc: place.getDescription(), imageName: place.getImageName(), webLink: place.getWebLink(), type: place.getType(), height: place.getHeight())
            annotations.append(annotation)
        }
        return annotations
    }

    override func viewWillLayoutSubviews() {
        //this initializes settings button in this controller
        let _ = SettingsButton(cView:self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    func getDocumentsDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func fileInDocumentsDirectory(fileName:String) -> String {
        let fileName = getDocumentsDirectory().stringByAppendingPathComponent(fileName)
        return fileName
    }
    
    func showSettingsController(){
        let settingsController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("settings") as! SettingsController
        self.presentViewController(settingsController, animated: true, completion: nil)
    }

}
