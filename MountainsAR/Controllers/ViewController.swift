//
//  ViewController.swift
//  MountainsAR
//
//  Created by Apple on 9/12/16.
//  Copyright © 2016 pagodalabs. All rights reserved.
//

import UIKit
import MapKit
import SwiftLoader
import AFMActionSheet

class ViewController: UIViewController,CLLocationManagerDelegate{
    
    var locationManager: CLLocationManager!
    
    let arView = TestAnnotationView()
    override func viewDidLoad() {
        super.viewDidLoad()

        if(CLLocationManager.locationServicesEnabled()){
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        let location = locations.last! as CLLocation
        let placesLoader = PlacesLoader(myView: self, myLocation: location)
        placesLoader.getPlaces()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.Denied{
            showAlertForLocation()
        } else if status == CLAuthorizationStatus.Restricted{
            showAlertForLocation()
        }
    }
    
    func showAlertForLocation(){
        let alert = UIAlertController(title: "Enable Location", message: "GPS access is restricted.. Please enable GPS in the Settings", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Go to Settings", style: .Default, handler: {(alert:UIAlertAction!) in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
            
        }))
        presentViewController(alert, animated: true, completion: nil)

    }
}

protocol Settings{
    func showSettingsController()
}

